import numpy
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_selection import mutual_info_regression


# if string convert to a number
def convert_to_int(i):
    if isinstance(i, str):
        return {
            "Yes": 1,
            "No": 0,
            "Bad": 0,
            "Medium": 1,
            "Good": 2
        }[i]
    else:
        return i


# convert dict to numpy array
def nparray_from_dict(data):
    list = [[convert_to_int(i) for i in dict.values()] for dict in data]
    return numpy.array(list)

# calc and print mutual information.
def print_mutual(data):
    npdata = nparray_from_dict(data)
    x_std = StandardScaler().fit_transform(npdata)
    print("MUTUAL INFORMATIONS:")
    print(str(data[0].keys()))
    print(str(mutual_info_regression(x_std, x_std[:,[0]])))








