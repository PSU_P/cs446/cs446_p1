import csv
import sys
from tree import Tree
from bagging import TreeBagging
from random_forest import RandomForest
import numpy
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import mutual_info_classif
import mi
from isqrt import isqrt


def standarize(data):
    for datum, index in zip(data, range(0, len(data))):
        new = {}
        for x in datum:
            if x == 'ShelveLoc' or x == 'Urban' or x == 'US':
                new[x] = datum[x]
            else:
                new[x] = float(datum[x])
        data[index] = new
    for feature in ['Sales', 'CompPrice', 'Income', 'Advertising', 'Population', 'Price', 'Age', 'Education']:
        maxval = max(data, key=lambda x: x[feature])[feature]
        for datum in data:
            datum[feature] = (datum[feature] / maxval)
    return data


def read_data(path):
    data = []
    reader = csv.DictReader(open(path, encoding='utf-8'))
    for row in reader:
        data.append(row)
    return standarize(data)


# cross validate to find different optimal alpha values.
def cv_alpha(traindata, train_features, predict, stop, folds, alphas):
    n = len(traindata) // folds # split the data into folds
    partitioned = [traindata[i:i + n] for i in range(0, len(traindata), n)]
    trees = []

    # for each fold assign one test and other train.
    for i in range(folds):
        Tree.progress(i, folds-1, "training on k-fold - 1 partitions")
        train = []
        for part, index in zip(partitioned, range(len(partitioned))):
            if index == i:
                continue
            train += part
        trees.append(Tree(train, train_features, predict, mse=None, stop_cond=stop, height=10, complexity=alphas[i]))
        trees[i].split() #train each tree
    best = None #find the best alpha
    for tree, index in zip(trees, range(len(trees))):
        tree.prune(partitioned[index], 14)
        error = Tree.compute_error(tree.test(partitioned[index]), partitioned[index], tree.predict_feature)
        if best is None or best[1] > error: # if new alpha is better then save to best.
            best = (tree.alpha, error)

    return best[0]




def main():
    sys.setrecursionlimit(2000)

    data = read_data("../data/Carseats.csv")

    mi.print_mutual(data)

    testlen = len(data) // 5
    testset = data[:testlen]
    trainset = data[testlen:]

    train_features = ['CompPrice', 'Income', 'Advertising', 'Population', 'Price', 'ShelveLoc', 'Age',
                          'Education', 'Urban', 'US']
    predict = 'Sales'

    # single tree.
    tree = Tree(trainset, train_features, predict, stop_cond=50, height=1000)
    tree.split()
    prediction = tree.test(testset)
    error = Tree.compute_error(prediction, testset, predict)
    Tree.printtree(tree)
    print("Single Tree Error " + str(error))

    # single pruned tree
    alpha = cv_alpha(trainset, train_features, predict, 1, 2, [.00007, .00005])  # cross fold yeilds bad results
    alpha = 0.00007 # here is the optimal alpha value
    train_test_len = len(trainset) // 5
    train_test = trainset[:train_test_len]  # partition train set into test and train in order to prune.
    train_train = trainset[train_test_len:]
    prune_tree = Tree(train_train, train_features, predict, mse=None, stop_cond=5, height=10, complexity=alpha)
    prune_tree.split()
    prune_tree.prune(train_test, 20)
    error = Tree.compute_error(prune_tree.test(testset), testset, predict)
    print("pruned tree error "+ str(error) + " alpha :"+str(alpha))

    # random forest of different feature bag sizes.
    for feature_size in [3,4,5,6,7,8,9,10]:
        forest = RandomForest(100, trainset, train_features, predict, feature_size, stop_cond=50, height=3)
        forest.train()
        prediction = forest.test(testset)
        error = Tree.compute_error(prediction, testset, predict)
        print("Random Forest Error (features "+ str(feature_size)+ "): " + str(error))


    # bagging.
    bagging = TreeBagging(100, trainset, train_features, predict, stop_cond=50, height=1000)
    bagging.train()
    prediction = bagging.test(testset)
    error = Tree.compute_error(prediction, testset, predict)
    print("Bagging error: " + str(error))


if __name__ == "__main__":
    main()
