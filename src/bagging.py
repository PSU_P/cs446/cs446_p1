from tree import Tree
import random
import sys

# this is a class that trains a set of trees with different (bootstrap sampling) train sets.


class TreeBagging(object):

    # size --> number of trees to grow.
    # train --> the train data
    # train_features -->features to split on.
    # predict_feature --> feature to predict.
    # stop_cond --> maximum number of data points in a region.
    # height --> height limit of the trees.
    def __init__(self, size, train, train_features, predict_feature, stop_cond, height):
        self.data = train
        self.bag_size = size
        self.trees = TreeBagging.init_trees(train, size, train_features, predict_feature, stop_cond, height)

    # samples train data and initializes trees.
    @staticmethod
    def init_trees(data, size, train_features, predict_feature, stop, height):

        # create the train sets
        base_trainsets = []
        for base_learner in range(size):
            base_trainsets.append([])
            for datum in range(len(data)):
                base_trainsets[base_learner].append(data[random.randint(0, len(data) - 1)])

        # initialize a list of trees.
        base_learners = []
        for base_train_set in base_trainsets:
            base_tree = Tree(base_train_set, train_features, predict_feature, stop_cond=stop, height=height)
            base_learners.append(base_tree)

        return base_learners

    # train the bagged trees.
    def train(self):
        for tree, index in zip(self.trees, range(len(self.trees))):
            Tree.progress(index+1, len(self.trees), suffix="Training Bagged Trees")
            tree.split()

    # test the bagged trees.
    def test(self, testset):
        predictions = []
        for testpoint in testset:
            average_predict = 0.0
            for tree in self.trees:  # find the average between all predictions.
                average_predict += tree.predict(testpoint) / len(self.trees)
            predictions.append(average_predict)
        return predictions
