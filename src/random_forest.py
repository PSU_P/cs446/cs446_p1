import random
from tree import Tree
import sys

class RandomForest(object):
    def __init__(self, size, traindata, train_features, predict_feature, feature_bag,  stop_cond, height):
        self.forest = RandomForest.plant_forest(size, traindata, train_features, predict_feature, feature_bag, stop_cond, height)

    # sample a set of features to split on and plant trees on those samples features.
    @staticmethod
    def plant_forest(size, traindata, train_features, predict_feature, feature_bag, stop_cond, height):
        subset_size = feature_bag
        trees = []
        for tree in range(size):
            subset = []
            for i in range(subset_size):
               subset.append(train_features[random.randint(0, len(train_features) - 1)])
            trees.append(Tree(traindata, subset, predict_feature, stop_cond=stop_cond, height=height))
        return trees

    # grow the forest on the train set.
    def train(self):
        for tree, index in zip(self.forest, range(len(self.forest))):
            tree.split()
            Tree.progress(index + 1, len(self.forest), suffix="Growing the forest")

    # test the forest averaging there predictions.
    def test(self, testset):
        predictions = []
        for datum in testset:
            predict_value = 0.0
            for tree in self.forest:
                predict_value += tree.predict(datum) / len(self.forest)
            predictions.append(predict_value)
        return predictions





