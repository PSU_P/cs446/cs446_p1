import math
from itertools import combinations
import sys
import random


class Tree(object):
    stop = 40  # default stop condition is 40 data points in each section.
    alpha = 1  # default alpha value to prune on.

    def __init__(self, data, train_features, predict,  mse=None, stop_cond=stop, height=1500, complexity=alpha):
        self.stop = stop_cond
        self.height_limit = height
        self.alpha = complexity
        self.data = data
        self.train_features = train_features
        self.predict_feature = predict
        self.left = None
        self.right = None
        self.split_feature = None
        self.split_value = None
        self.avg = None
        self.mse = mse
        self.leaf = False

    # prints the progress of a process
    @staticmethod
    # Vladimir Ignatyev  https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    def progress(count, total, suffix=''):
        bar_len = 60

        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('\r[%s] %s%s ...%s' % (bar, percents, '%', suffix))
        sys.stdout.flush()
        if count == total:
            print("")

    # given a list of predictions and data with the target index,
    # the average mse is computed.
    @staticmethod
    def compute_error(predictions, data, predict_feature):
        error = 0.0
        for predict, actual in zip(predictions, data):
            error += (actual[predict_feature] - predict) ** 2 / len(predictions)
        return error

    # prints the split values for a tree.
    @staticmethod
    def printtree(root):
        nodes = []
        root.get_tree(nodes, 0, "root")
        for i in nodes:
            print(str(i))

    # calculates the height of a tree.
    def height(self):
        if self.leaf:
            return 1
        else:
            lh = self.left.height()
            rh = self.right.height()
            if lh < rh:
                return 1 + rh
            else:
                return 1 + lh

    # gets the split values and returns a list of them. "str" is the name of root node.
    def get_tree(self, data, level, str):
        if self.leaf:
            if level == len(data):
                data.append([(str, "Leaf")])
            else:
                data[level].append((str, "Leaf"))
            return
        if level == len(data):
            data.append([(str, self.split_feature, self.split_value)])
        else:
            data[level].append((str, self.split_feature, self.split_value))
        self.left.get_tree(data, level + 1, "Left")
        self.right.get_tree(data, level + 1, "Right")

    # averages the target values to make a prediction on the leaf.
    def average(self):
        sales = list(map(lambda d: d[self.predict_feature], self.data))
        self.avg = sum(i / len(sales) for i in sales)

    # computes the mse of a leaf given the values x to predict.
    def MSE(self, x):
        sales = list(map(lambda d: d[self.predict_feature], x))
        if len(sales) == 0:
            return 0
        avg = sum(i / len(sales) for i in sales)
        return sum((i - avg) ** 2 for i in sales)

    # finds the best split value of "feature" on the particular "data"
    def feature_best_split(self, data, feature):
        list.sort(data, key=lambda x: x[feature])
        # to save the best results
        partition = (None, None, None, None, None)
        for i in data:
            # if feature value is the same then get the next value.
            if partition[0] is not None and i[feature] == partition[1]:
                continue
            feature_val = i[feature]
            # determine if value is a string or a number.
            if isinstance(feature_val, str):
                right = list(filter(lambda x: x[feature] == feature_val, data))  # condition met
                left = list(filter(lambda x: x[feature] != feature_val, data))  # condition not met
            else:
                right = list(filter(lambda x: x[feature] >= feature_val, data))  # condition met
                left = list(filter(lambda x: x[feature] < feature_val, data))  # condition not met
            # compute the errors of the right region and the left region on the left and right data.
            error = self.MSE(right) + self.MSE(left)
            # if no best results or partition error is less then save the result.
            if partition[2] is None or error < (partition[2]):
                partition = (feature, i[feature], error, left, right)
        # once all values where evaluated for the particular feature return the best result
        return partition

    # given the features to split on, self.train_features, find the best feature and split value that
    # minimizes the mse .
    def compute_best_split(self, data):
        best_split = None
        for feature in self.train_features:
            best_feature_split = self.feature_best_split(data, feature)
            if best_split is None or best_feature_split[2] < best_split[2]:
                best_split = best_feature_split
        return best_split

    # uses compute best split to find the best split value and feature. Recursively splits until
    # region size limit is reached or height limit is reached.
    def split(self, level=1):
        if len(self.data) <= self.stop or level == self.height_limit:
            self.leaf = True
            self.average()  # compute the average of leaf.
            return
        best_split = self.compute_best_split(self.data)
        self.split_feature = best_split[0]
        self.split_value = best_split[1]
        self.left = Tree(best_split[3], self.train_features, self.predict_feature, best_split[2], stop_cond=self.stop,
                         complexity=self.alpha, height=self.height_limit)
        self.right = Tree(best_split[4], self.train_features, self.predict_feature, best_split[3], stop_cond=self.stop,
                          complexity=self.alpha, height=self.height_limit)
        self.right.split(level=level + 1)
        self.left.split(level=level + 1)

    # for a particular point return (recursively) the predicted value for it.
    def predict(self, point):
        if self.leaf:
            return self.avg
        if isinstance(self.split_value, str):
            if point[self.split_feature] == self.split_value:
                return self.right.predict(point)
            else:
                return self.left.predict(point)
        else:
            if point[self.split_feature] >= self.split_value:
                return self.right.predict(point)
            else:
                return self.left.predict(point)

    # given a test set computes the predictions and returns them in a list ( in order ) .
    def test(self, data):
        estimates = []
        for point in data:
            estimates.append(self.predict(point))
        return estimates

    # gets the subtrees in a tree and returns them in a list.
    # This is used in the pruning process.
    def get_sub_trees(self, list):
        if not self.leaf:
            list.append(self)
            self.left.get_sub_trees(list)
            self.right.get_sub_trees(list)
        else:
            return

    # counts the number of leaf in a tree. (complexity)
    def count_leafs(self):
        if self.leaf:
            return 1
        else:
            return self.right.count_leafs() + self.left.count_leafs()

    # using count_leafs,  computes the complexity error.
    def calc_pruned_error(self, predictions, data):
        error = 0.0
        for predict, actual in zip(predictions, data):
            error += (actual[self.predict_feature] - predict) ** 2 / len(predictions)
        return error + self.alpha * self.count_leafs()

    def cut_branches(self, branches):
        for branch in branches:
            branch.leaf = True
            branch.average()

    def stich_branches(self, branches):
        for branch in branches:
            branch.leaf = False

    # Prunes a tree. test_set      -> from train set in order to test complexity.
    #                num_of_splits -> number of random subtrees to test for optimal pruning.
    def prune(self, test_set, num_of_splits):
        # calculate the complexity error for the original tree.
        best_pruned_mse = self.calc_pruned_error(self.test(test_set), test_set)
        sub_trees = []
        self.get_sub_trees(sub_trees)  # get all the sub-trees in the tree.
        sub_trees = random.sample(sub_trees, num_of_splits)  # take a sample from the
        # sub-trees of size num_of_splits
        best_prune = None
        for i in range(1, len(sub_trees) + 1):
            Tree.progress(i, len(sub_trees), "finding best branches to cut for alpha=" + str(self.alpha))
            for comb in combinations(sub_trees, i):  # for all the different combinations to cut branches find smallest complexity error
                self.cut_branches(comb)
                new_mse = self.calc_pruned_error(self.test(test_set), test_set)
                if new_mse < best_pruned_mse:  # if error is less save the result.
                    best_pruned_mse = new_mse
                    best_prune = comb
                self.stich_branches(comb) # set back to original state to test other pruning combinations.
        self.cut_branches(best_prune)
        return  # return the best pruned tree for the particular alpha value.
